package com.memory.colaborador.controller;

import com.memory.colaborador.exceptions.ApiErrorException;
import com.memory.colaborador.service.ColaboradorService;
import com.memory.colaborador.service.DTO.Response;
import com.memory.colaborador.service.DTO.request.CriaColaboradorRequestDTO;
import com.memory.colaborador.service.DTO.request.DadosColaboradorPaginadoDTO;
import com.memory.colaborador.service.DTO.request.EditaColaboradorRequestDTO;
import com.memory.colaborador.service.DTO.response.ColaboradorDetalheResponseDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/colaborador")
public class ColaboradorController {

    private final ColaboradorService colaboradorService;

    @GetMapping()
    public Response<Page<ColaboradorDetalheResponseDTO>> getColaboradores(
            @RequestParam(value = "anoContratacao", required = false) String anoContratacao,
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "size", defaultValue = "10") Integer size
    ) {
        Response<Page<ColaboradorDetalheResponseDTO>> response = new Response<>();
        DadosColaboradorPaginadoDTO paginacaoRequestData = new DadosColaboradorPaginadoDTO();
        paginacaoRequestData.setPage(page);
        paginacaoRequestData.setSizePage(size);
        paginacaoRequestData.setAnoContratacao(anoContratacao);
        response.setOk();
        response.setData(colaboradorService.buscaColaboradoresPaginado(paginacaoRequestData));
        return response;
    }

    @DeleteMapping()
    public Response<Void> deleteColaborador(
            @RequestBody CriaColaboradorRequestDTO data
    ) throws ApiErrorException {
        Response<Void> response = new Response<>();
        colaboradorService.deletaColaborador(data);
        response.setOk();
        return response;
    }

    @PutMapping("/atualizar")
    public Response<ColaboradorDetalheResponseDTO> editaColaborador(
            @RequestBody EditaColaboradorRequestDTO data
    ) throws ApiErrorException {
        Response<ColaboradorDetalheResponseDTO> response = new Response<>();
        response.setData(colaboradorService.editaDadosColaborador(data));
        response.setOk();
        return response;
    }

    @PostMapping("/criar")
    public Response<ColaboradorDetalheResponseDTO> criarColaborador(
            @RequestBody CriaColaboradorRequestDTO data
    ) throws ApiErrorException {
        Response<ColaboradorDetalheResponseDTO> response = new Response<>();
        response.setOk();
        response.setData(colaboradorService.criarColaborador(data));
        return response;
    }
}
