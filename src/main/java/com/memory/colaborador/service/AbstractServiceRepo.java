package com.memory.colaborador.service;

import com.memory.colaborador.repository.GenericRepository;

import java.io.Serializable;
import java.util.Optional;

public abstract class AbstractServiceRepo<R extends GenericRepository<T, I>, T, I extends Serializable> extends AbstractService {
    protected R repository;

    public AbstractServiceRepo(R repository) {
        this.repository = repository;
    }

    public T save(T entity) {
        return repository.save(entity);
    }

    public Optional<T> findById(I id) {
        return repository.findById(id);
    }
}
