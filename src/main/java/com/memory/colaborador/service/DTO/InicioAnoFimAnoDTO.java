package com.memory.colaborador.service.DTO;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class InicioAnoFimAnoDTO {
    private LocalDateTime inicioAno;
    private LocalDateTime fimAno;
}
