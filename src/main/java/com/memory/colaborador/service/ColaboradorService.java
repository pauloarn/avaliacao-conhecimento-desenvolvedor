package com.memory.colaborador.service;

import com.memory.colaborador.enums.FuncoesEnum;
import com.memory.colaborador.enums.MessageEnum;
import com.memory.colaborador.exceptions.ApiErrorException;
import com.memory.colaborador.model.Colaborador;
import com.memory.colaborador.repository.ColaboradorRepository;
import com.memory.colaborador.service.DTO.InicioAnoFimAnoDTO;
import com.memory.colaborador.service.DTO.request.CriaColaboradorRequestDTO;
import com.memory.colaborador.service.DTO.request.DadosColaboradorPaginadoDTO;
import com.memory.colaborador.service.DTO.request.EditaColaboradorRequestDTO;
import com.memory.colaborador.service.DTO.response.ColaboradorDetalheResponseDTO;
import com.memory.colaborador.utils.DateUtils;
import com.memory.colaborador.utils.StringUtils;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Log4j2
public class ColaboradorService extends AbstractServiceRepo<ColaboradorRepository, Colaborador, Long> {

    @Autowired
    public ColaboradorService(ColaboradorRepository repository) {
        super(repository);
    }

    public Page<ColaboradorDetalheResponseDTO> buscaColaboradoresPaginado(DadosColaboradorPaginadoDTO paginacaoRequest) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Colaborador> criteriaQuery = criteriaBuilder.createQuery(Colaborador.class);
        List<Predicate> andPredicates = new ArrayList<>();

        Root<Colaborador> from = criteriaQuery.from(Colaborador.class);
        if (Objects.nonNull(paginacaoRequest.getAnoContratacao())) {
            InicioAnoFimAnoDTO inicioFimAno = DateUtils.getStringAnoToInicioFimAno(paginacaoRequest.getAnoContratacao());
            andPredicates.add(criteriaBuilder.greaterThanOrEqualTo(from.get("dataAdmissao"), inicioFimAno.getInicioAno()));
            andPredicates.add(criteriaBuilder.lessThanOrEqualTo(from.get("dataAdmissao"), inicioFimAno.getFimAno()));
        }

        criteriaQuery = criteriaQuery.select(from).where(andPredicates.toArray(new Predicate[0]));
        TypedQuery<Colaborador> query = em.createQuery(criteriaQuery);
        CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(Long.class);
        Root<Colaborador> countRoot = countQuery.from(criteriaQuery.getResultType());
        countRoot.alias(from.getAlias());
        countQuery.select(criteriaBuilder.count(countRoot)).where(criteriaQuery.getRestriction());
        Long totalColaboradores = em.createQuery(countQuery).getSingleResult();
        log.info("Total de colaboradores encontrados: {}", totalColaboradores);

        int page = paginacaoRequest.getPage();
        int sizePage = paginacaoRequest.getSizePage();
        Pageable pageable = generatePageable(page, sizePage);
        page = pageable.getPageNumber();
        query.setFirstResult(page * sizePage);
        query.setMaxResults(sizePage);
        log.info("Buscando os colaboradores");
        List<Colaborador> listaColaborador = query.getResultList();
        log.info("Buscar concluida");
        List<ColaboradorDetalheResponseDTO> listaDossieResultado = listaColaborador.stream().map(ColaboradorDetalheResponseDTO::new).collect(Collectors.toList());

        log.info("Busca e conversão dos colaboradores concluída, enviando colaboradores");

        return new PageImpl<>(listaDossieResultado, pageable, totalColaboradores);
    }

    public Colaborador buscaColaboradorPorCpf(String cpf) throws ApiErrorException {
        String cpfLimpo = StringUtils.getCpfOnlyNumbers(cpf);
        Optional<Colaborador> colaborador = repository.findColaboradorporCpf(cpfLimpo);
        if (colaborador.isPresent()) {
            return colaborador.get();
        }
        throw new ApiErrorException(HttpStatus.NOT_FOUND, MessageEnum.COLABORADOR_NAO_ENCONTRADO);
    }

    public ColaboradorDetalheResponseDTO editaDadosColaborador(EditaColaboradorRequestDTO dadosColaborados) throws ApiErrorException {
        Colaborador colaborador = buscaColaboradorPorCpf(dadosColaborados.getCpf());
        List<Colaborador> subordinados = new ArrayList<>();
        colaborador.setNome(dadosColaborados.getNome());
        colaborador.setFuncao(dadosColaborados.getFuncao());
        colaborador.setRemuneracao(StringUtils.getBigDecimalFromFormatedString(dadosColaborados.getRemuneracao()));
        colaborador.setDataAdmissao(DateUtils.formatedStringToLocalDateTime(dadosColaborados.getDataAdmissao()));
        colaborador.getListaSubordinados().clear();
        if (Objects.nonNull(dadosColaborados.gerente)) {
            String gerenteCpf = StringUtils.getCpfOnlyNumbers(dadosColaborados.getGerente());
            colaborador.setGerente(buscaColaboradorPorCpf(gerenteCpf));
        }
        for (String subordinadoCpf : dadosColaborados.getListaSubordinados()) {
            subordinados.add(buscaColaboradorPorCpf(subordinadoCpf));
        }
        validaPodeSalvarColaborador(colaborador);
        colaborador.setListaSubordinados(subordinados);
        salvarColaborador(colaborador);
        return new ColaboradorDetalheResponseDTO(colaborador);
    }

    private void validaPodeSalvarColaborador(Colaborador colaborador) throws ApiErrorException {
        if (colaborador.getFuncao().equals(FuncoesEnum.COLABORADOR) && colaborador.getListaSubordinados().size() > 0) {
            throw new ApiErrorException(HttpStatus.BAD_REQUEST, MessageEnum.NECESSARIO_SER_GERENTE_PARA_TER_SUBORDINADOS);
        }
        for (Colaborador subordinado : colaborador.getListaSubordinados()) {
            if (validaColaboradorJaTemGerente(subordinado.getCpf())) {
                throw new ApiErrorException(HttpStatus.BAD_REQUEST, MessageEnum.COLABORADOR_JA_POSSUI_GERENTE, List.of(subordinado.getCpf()));
            }
        }
        Colaborador gerenteDoColaborador = colaborador.getGerente();
        if (Objects.nonNull(colaborador.getGerente()) && !gerenteDoColaborador.getFuncao().equals(FuncoesEnum.GERENTE)) {
            throw new ApiErrorException(HttpStatus.BAD_REQUEST, MessageEnum.COLABORADOR_NAO_E_GERENTE);
        }
    }

    private boolean validaColaboradorJaTemGerente(String cpfColaborador) throws ApiErrorException {
        Colaborador colaborador = buscaColaboradorPorCpf(cpfColaborador);
        return Objects.nonNull(colaborador.getGerente());
    }

    private boolean validaGerenteColaborador(Colaborador subordinado, Colaborador gerente) {
        return Objects.isNull(subordinado.getGerente()) || subordinado.getGerente().equals(gerente);
    }

    public void deletaColaborador(CriaColaboradorRequestDTO data) throws ApiErrorException {
        Colaborador colaborador = buscaColaboradorPorCpf(data.getCpf());
        repository.delete(colaborador);
    }

    public ColaboradorDetalheResponseDTO criarColaborador(CriaColaboradorRequestDTO criarColaboradorData) throws ApiErrorException {
        Colaborador novoColaborador = criarColaboradorData.toClass();
        List<Colaborador> subordinados = new ArrayList<>();
        if (Objects.nonNull(criarColaboradorData.getGerente()) && !criarColaboradorData.getGerente().isEmpty()) {
            novoColaborador.setGerente(buscaColaboradorPorCpf(criarColaboradorData.getGerente()));
        }
        for (String subordinadoCpf : criarColaboradorData.getListaSubordinados()) {
            subordinados.add(buscaColaboradorPorCpf(subordinadoCpf));
        }
        novoColaborador.setListaSubordinados(subordinados);
        validaPodeSalvarColaborador(novoColaborador);
        salvarColaborador(novoColaborador);
        return new ColaboradorDetalheResponseDTO(novoColaborador);
    }

    public void salvarColaborador(Colaborador colaborador) {
        colaborador.setAtualizadoEm(DateUtils.dateToLocalDateTime(new Date()));
        repository.save(colaborador);
    }
}
