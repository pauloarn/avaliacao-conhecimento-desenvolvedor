package com.memory.colaborador.controller;

import com.memory.colaborador.exceptions.ApiErrorException;
import com.memory.colaborador.service.DTO.Response;
import com.memory.colaborador.service.DTO.request.EncontrarResponsavelConflitoRequestDTO;
import com.memory.colaborador.service.DTO.response.ColaboradorResponseDTO;
import com.memory.colaborador.service.ResolverConflitoService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/resolver-conflito")
public class ResponsavelConflitoController {
    private final ResolverConflitoService resolverConflitoService;

    @PostMapping()
    public Response<ColaboradorResponseDTO> buscaResponsavelResolverConflito(
            @RequestBody EncontrarResponsavelConflitoRequestDTO data
    ) throws ApiErrorException {
        Response<ColaboradorResponseDTO> response = new Response<>();
        response.setData(resolverConflitoService.encontraResponsavelColaboradores(data));
        response.setOk();
        return response;
    }
}
