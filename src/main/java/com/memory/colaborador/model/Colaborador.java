package com.memory.colaborador.model;

import com.memory.colaborador.config.Catalog;
import com.memory.colaborador.config.Schema;
import com.memory.colaborador.enums.FuncoesEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "colaborador", catalog = Catalog.POSTGRES, schema = Schema.COLABORADORES)
public class Colaborador extends BaseEntity {
    @Id
    @Column(name = "id")
    @SequenceGenerator(name = "idcolaborador_id_seq",
            sequenceName = "idcolaborador_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "idcolaborador_id_seq")
    private Long colaboradorId;

    @Column(name = "nome")
    private String nome;

    @Column(name = "cpf")
    private String cpf;

    @Column(name = "data_admissao")
    private LocalDateTime dataAdmissao;

    @Column(name = "funcao")
    @Enumerated(EnumType.STRING)
    private FuncoesEnum funcao;

    @Column(name = "remuneracao")
    private BigDecimal remuneracao;

    @OneToMany
    @JoinTable(
            name = "gerente_subordinado",
            joinColumns = @JoinColumn(name = "gerente_fk"),
            inverseJoinColumns = @JoinColumn(name = "subordinado_fk"),
            catalog = Catalog.POSTGRES,
            schema = Schema.COLABORADORES
    )
    private List<Colaborador> listaSubordinados;

    @OneToOne
    @JoinTable(
            name = "gerente_subordinado",
            joinColumns = @JoinColumn(name = "subordinado_fk"),
            inverseJoinColumns = @JoinColumn(name = "gerente_fk"),
            catalog = Catalog.POSTGRES,
            schema = Schema.COLABORADORES
    )
    private Colaborador gerente;

}
