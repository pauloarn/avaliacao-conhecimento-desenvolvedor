package com.memory.colaborador.utils;

import com.memory.colaborador.enums.MessageEnum;
import com.memory.colaborador.exceptions.ApiErrorException;
import com.memory.colaborador.service.DTO.InicioAnoFimAnoDTO;
import org.springframework.http.HttpStatus;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Objects;

public class DateUtils {

    public static LocalDateTime dateToLocalDateTime(Date date) {
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
    }

    public static LocalDateTime formatedStringToLocalDateTime(String string) throws ApiErrorException {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date date;
        try {
            date = format.parse(string);
        } catch (ParseException ex) {
            throw new ApiErrorException(HttpStatus.BAD_REQUEST, MessageEnum.FORMATACAO_DATA_INCORRETA);
        }
        return dateToLocalDateTime(date);
    }

    public static InicioAnoFimAnoDTO getStringAnoToInicioFimAno(String ano) {
        LocalDateTime inicioAno = LocalDateTime.of(Integer.parseInt(ano), Month.JANUARY, 01, 00, 00, 01);
        LocalDateTime fimAno = LocalDateTime.of(Integer.parseInt(ano), Month.DECEMBER, 31, 23, 59, 59);
        InicioAnoFimAnoDTO inicioFimAno = new InicioAnoFimAnoDTO();
        inicioFimAno.setInicioAno(inicioAno);
        inicioFimAno.setFimAno(fimAno);
        return inicioFimAno;
    }

    public static String formatDate(String pattern, LocalDateTime date) {
        if (Objects.isNull(date)) {
            return null;
        }
        return DateTimeFormatter.ofPattern(pattern).format(date);
    }
}
