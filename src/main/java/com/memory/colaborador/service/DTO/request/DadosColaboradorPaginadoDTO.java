package com.memory.colaborador.service.DTO.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DadosColaboradorPaginadoDTO {
    private String anoContratacao;
    private Integer page;
    private Integer sizePage;
}
