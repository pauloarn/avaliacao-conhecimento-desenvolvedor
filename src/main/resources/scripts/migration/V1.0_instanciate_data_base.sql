create schema colaboradores;
CREATE SEQUENCE colaboradores.idcolaborador_id_seq;

CREATE TABLE colaboradores.colaborador
(
    id            bigint    NOT NULL primary key default nextval('idcolaborador_id_seq'),
    nome          varchar   NOT NULL,
    cpf           varchar   NOT NULL UNIQUE,
    data_admissao timestamp NOT NULL,
    funcao        varchar   NOT NULL,
    remuneracao   decimal   NOT NULL,
    criado_em     timestamp NOT NULL,
    atualizado_em timestamp NOT NULL
);


CREATE TABLE colaboradores.gerente_subordinado
(
    gerente_fk     bigint not null,
    subordinado_fk bigint not null,
    CONSTRAINT GERENTE_SUBORDINADO_COLABORADOR_GERENTE FOREIGN KEY (gerente_fk) references colaboradores.colaborador (id),
    CONSTRAINT GERENTE_SUBORDINADO_COLABORADOR_SUBORDINADO FOREIGN KEY (subordinado_fk) references colaboradores.colaborador (id)
);