package com.memory.colaborador.enums;

public enum FuncoesEnum {
    COLABORADOR("COLABORADOR"),
    GERENTE("GERENTE");


    private String descricao;

    FuncoesEnum(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return this.descricao;
    }
}
