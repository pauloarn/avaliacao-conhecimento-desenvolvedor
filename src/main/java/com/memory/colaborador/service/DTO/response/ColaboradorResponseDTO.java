package com.memory.colaborador.service.DTO.response;

import com.memory.colaborador.model.Colaborador;
import com.memory.colaborador.utils.DateUtils;
import com.memory.colaborador.utils.StringUtils;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ColaboradorResponseDTO {
    private Long colaboradorId;
    private String cpf;
    private String nome;
    private String dataAdmissao;
    private String remuneracao;
    private String funcao;

    public ColaboradorResponseDTO(Colaborador colaborador) {
        this.colaboradorId = colaborador.getColaboradorId();
        this.cpf = StringUtils.formatarCPF(colaborador.getCpf());
        this.dataAdmissao = DateUtils.formatDate("dd/MM/yyyy", colaborador.getDataAdmissao());
        this.nome = colaborador.getNome();
        this.remuneracao = colaborador.getRemuneracao().toString();
        this.funcao = colaborador.getFuncao().getDescricao();
    }
}
