package com.memory.colaborador.service.DTO.request;


import com.memory.colaborador.enums.FuncoesEnum;
import com.memory.colaborador.exceptions.ApiErrorException;
import com.memory.colaborador.model.Colaborador;
import com.memory.colaborador.utils.DateUtils;
import com.memory.colaborador.utils.StringUtils;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class CriaColaboradorRequestDTO {
    @NotNull
    public String cpf;
    @NotNull
    public String nome;
    @NotNull
    public String dataAdmissao;
    @NotNull
    public FuncoesEnum funcao;
    @NotNull
    public String remuneracao;
    public String gerente;
    @NotNull
    public List<String> listaSubordinados;


    public Colaborador toClass() throws ApiErrorException {
        Colaborador colaborador = new Colaborador();
        colaborador.setNome(this.getNome());
        colaborador.setCpf(StringUtils.getCpfOnlyNumbers(this.getCpf()));
        colaborador.setFuncao(this.getFuncao());
        colaborador.setRemuneracao(StringUtils.getBigDecimalFromFormatedString(this.getRemuneracao()));
        colaborador.setDataAdmissao(DateUtils.formatedStringToLocalDateTime(this.getDataAdmissao()));
        colaborador.setCriadoEm(DateUtils.dateToLocalDateTime(new Date()));
        return colaborador;
    }

}
