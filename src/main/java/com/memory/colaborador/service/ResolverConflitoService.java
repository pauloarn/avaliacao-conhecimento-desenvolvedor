package com.memory.colaborador.service;

import com.memory.colaborador.enums.MessageEnum;
import com.memory.colaborador.exceptions.ApiErrorException;
import com.memory.colaborador.model.Colaborador;
import com.memory.colaborador.service.DTO.request.EncontrarResponsavelConflitoRequestDTO;
import com.memory.colaborador.service.DTO.response.ColaboradorResponseDTO;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Log4j2
@Service
public class ResolverConflitoService extends AbstractService {
    private final ColaboradorService colaboradorService;

    public ResolverConflitoService(ColaboradorService colaboradorService) {
        this.colaboradorService = colaboradorService;
    }

    public ColaboradorResponseDTO encontraResponsavelColaboradores(EncontrarResponsavelConflitoRequestDTO envolvidosConflito) throws ApiErrorException {
        Colaborador primeiroColaborador = colaboradorService.buscaColaboradorPorCpf(envolvidosConflito.getPrimeiroColaborador());
        Colaborador segundoColaborador = colaboradorService.buscaColaboradorPorCpf(envolvidosConflito.getSegundoColaborador());
        validaGerentesColaboradores(primeiroColaborador, segundoColaborador);
        Colaborador gerenteComum = encontraGerenteComum(primeiroColaborador, segundoColaborador);
        return new ColaboradorResponseDTO(gerenteComum);
    }

    private Colaborador encontraGerenteComum(Colaborador primeiroColaborador, Colaborador segundoColaborador) throws ApiErrorException {
        Colaborador gerente = null;
        Colaborador gerenteTestado = primeiroColaborador.getGerente();
        Set<Colaborador> colaboradoresSubordinados = new HashSet<>();
        while (Objects.isNull(gerente)) {
            addColaborador(colaboradoresSubordinados, gerenteTestado);

            if (colaboradoresSubordinados.contains(primeiroColaborador) && colaboradoresSubordinados.contains(segundoColaborador)) {
                gerente = gerenteTestado;
            }
            if (Objects.nonNull(gerenteTestado.getGerente())) {
                gerenteTestado = gerenteTestado.getGerente();
                colaboradoresSubordinados.clear();
                continue;
            }

            if (Objects.nonNull(segundoColaborador.getGerente())) {
                gerenteTestado = segundoColaborador.getGerente();
                colaboradoresSubordinados.clear();
                continue;
            }
            throw new ApiErrorException(HttpStatus.BAD_REQUEST, MessageEnum.NAO_ENCONTRADO_GERENTE_COMUM);
        }
        return gerente;
    }


    private void addColaborador(Set<Colaborador> listaColaborador, Colaborador gerente) {
        List<Colaborador> listaSubordinados = gerente.getListaSubordinados();
        listaColaborador.addAll(listaSubordinados);
        for (var colaborador : listaSubordinados) {
            addColaborador(listaColaborador, colaborador);
        }
    }

    private void validaGerentesColaboradores(Colaborador primeiroColaborador, Colaborador segundoCdolaborador) throws ApiErrorException {
        if (Objects.isNull(primeiroColaborador.getGerente())) {
            throw new ApiErrorException(HttpStatus.BAD_REQUEST, MessageEnum.COLABORADOR_NAO_POSSUI_GERENTE, List.of(primeiroColaborador.getNome()));
        }
        if (Objects.isNull(segundoCdolaborador.getGerente())) {
            throw new ApiErrorException(HttpStatus.BAD_REQUEST, MessageEnum.COLABORADOR_NAO_POSSUI_GERENTE, List.of(segundoCdolaborador.getNome()));
        }
    }
}
