package com.memory.colaborador.service.DTO.request;

import com.memory.colaborador.enums.FuncoesEnum;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
public class EditaColaboradorRequestDTO {

    @NotNull
    public String cpf;
    @NotNull
    public String nome;
    @NotNull
    public String dataAdmissao;
    @NotNull
    public FuncoesEnum funcao;
    @NotNull
    public String remuneracao;
    public String gerente;
    @NotNull
    public List<String> listaSubordinados;
}
