package com.memory.colaborador.service.DTO.response;

import com.memory.colaborador.model.Colaborador;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Getter
@Setter
public class ColaboradorDetalheResponseDTO extends ColaboradorResponseDTO {
    private List<ColaboradorResponseDTO> subordinados;
    private ColaboradorResponseDTO gerente;

    public ColaboradorDetalheResponseDTO(Colaborador colaborador) {
        super(colaborador);
        if (Objects.nonNull(colaborador.getGerente())) {
            this.gerente = new ColaboradorResponseDTO(colaborador.getGerente());
        }
        this.subordinados = colaborador.getListaSubordinados().stream().map(ColaboradorResponseDTO::new).collect(Collectors.toList());
    }
}
