package com.memory.colaborador.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.memory.colaborador.enums.MessageEnum;
import com.memory.colaborador.exceptions.ApiErrorException;
import com.memory.colaborador.exceptions.BadRequestException;
import com.memory.colaborador.exceptions.NotFoundException;
import com.memory.colaborador.utils.JsonUtils;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;

import static com.memory.colaborador.utils.ExceptionUtils.logException;

@Log4j2
public abstract class AbstractService extends AbstractConverter {

    @Autowired
    public ApplicationEventPublisher publisher;

    private JsonUtils jsonUtils;

    @PersistenceContext
    public EntityManager em;

    @Autowired
    public final void setJsonUtils(JsonUtils jsonUtils) {
        this.jsonUtils = jsonUtils;
    }

    protected void throwBadRequest(MessageEnum messageEnum) throws BadRequestException {
        throw new BadRequestException(messageEnum);
    }

    protected void throwNotFound(MessageEnum messageEnum) throws NotFoundException {
        throw new NotFoundException(messageEnum);
    }

    protected void throwUnprocessableEntity(MessageEnum messageEnum) throws ApiErrorException {
        throw ApiErrorException.builder()
                .unprocessabelEntity()
                .messageEnum(messageEnum)
                .build();
    }

    protected Supplier<NotFoundException> supplierThrowNotFound(MessageEnum messageEnum) {
        return () -> new NotFoundException(messageEnum);
    }

    public Pageable generatePageable(Integer page, Integer size) {
        if (page == null || size == null)
            return null;
        return generatePageable(page, size, Sort.unsorted());
    }

    public Pageable generatePageable(Integer page, Integer size, Sort sort) {
        if (page == null || size == null)
            return null;
        if (page - 1 > -1) page = page - 1;
        return PageRequest.of(page, size, sort);
    }

    public <T> Page<T> listToPage(List<T> listEntity, Pageable pageable) {
        List<T> output = new ArrayList<>();
        long start = pageable.getOffset();
        long end = Math.min(start + (long) pageable.getPageSize(), listEntity.size());
        if (start <= end) output = listEntity.subList((int) start, (int) end);
        return new PageImpl<>(output, pageable, listEntity.size());
    }

    public void logDto(String msg, Object dto) {
        try {
            log.debug(msg + ": {}", jsonUtils.objectToJson(dto));
        } catch (Exception e) {
            log.debug(msg + ": {}", dto);
            log.warn("Ocorreu um problema ao mostrar o dto em json: {}", dto.getClass().getSimpleName());
            logException(e);
        }
    }

    public void logDto(Object dto) {
        var className = dto.getClass().getSimpleName();
        this.logDto(String.format("Dados de %s", className), dto);
    }

    public <C> C clone(C clazz) throws IOException {
        return (C) jsonUtils.jsonToObject(jsonUtils.objectToJson(clazz), clazz.getClass());
    }

    public <C> Boolean isObjectEqual(C obj1, C obj2) throws JsonProcessingException {
        String obj1Json = jsonUtils.objectToJson(obj1);
        String obj2Json = jsonUtils.objectToJson(obj2);
        return obj1Json.equals(obj2Json);
    }

    public boolean isNull(Object obj) {
        return Objects.isNull(obj);
    }

    public boolean isNotNull(Object obj) {
        return Objects.nonNull(obj);
    }

    public boolean isEquals(Object objectLeft, Object objectRight) {
        return Objects.equals(objectLeft, objectRight);
    }

    public boolean isNotEquals(Object objectLeft, Object objectRight) {
        return !this.isEquals(objectLeft, objectRight);
    }
}
