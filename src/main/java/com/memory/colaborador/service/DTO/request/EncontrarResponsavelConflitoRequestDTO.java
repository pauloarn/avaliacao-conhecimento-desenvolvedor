package com.memory.colaborador.service.DTO.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EncontrarResponsavelConflitoRequestDTO {
    private String primeiroColaborador;
    private String segundoColaborador;
}
