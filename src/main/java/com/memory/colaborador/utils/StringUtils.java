package com.memory.colaborador.utils;

import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Component
public class StringUtils {

    public static BigDecimal getBigDecimalFromFormatedString(String formatedString) {
        String stringTrocandoPontoPorVirgula = formatedString.replace(",", ".");
        return new BigDecimal(stringTrocandoPontoPorVirgula).setScale(2, RoundingMode.DOWN);
    }

    public static String getCpfOnlyNumbers(String cpfFormatado) {
        return cpfFormatado.replaceAll("\\D+", "");
    }

    public static String leftPad(String texto, Integer tamanho, Character caracter) {
        if (texto.length() < tamanho) {
            StringBuilder sb = new StringBuilder(texto);
            for (int cont = 0; cont < (tamanho - texto.length()); cont++) {
                sb.insert(0, caracter);
            }
            return sb.toString();
        }
        return texto;
    }

    public static String formatarCPF(String cpf) {
        String cpfCompleto = leftPad(cpf, 11, '0');
        return cpfCompleto.substring(0, 3) + "." + cpfCompleto.substring(3, 6) + "." + cpfCompleto.substring(6, 9) + "-" + cpfCompleto.substring(9, 11);
    }
}
