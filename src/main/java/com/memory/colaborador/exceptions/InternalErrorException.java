package com.memory.colaborador.exceptions;

public class InternalErrorException extends Exception {
    public InternalErrorException(String message) {
        super(message);
    }
}