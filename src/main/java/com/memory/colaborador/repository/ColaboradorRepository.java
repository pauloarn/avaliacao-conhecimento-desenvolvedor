package com.memory.colaborador.repository;

import com.memory.colaborador.model.Colaborador;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ColaboradorRepository extends GenericRepository<Colaborador, Long> {
    @Query("select count(c.cpf) from Colaborador c")
    Long countColaboradores();

    @Query("select c from Colaborador c where c.cpf = :cpf")
    Optional<Colaborador> findColaboradorporCpf(
            @Param("cpf") String cpf
    );
}
