package com.memory.colaborador.service.DTO.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApagaColaboradorRequestDTO {
    private String cpfColaborador;
}
