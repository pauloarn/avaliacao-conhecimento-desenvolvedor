package com.memory.colaborador.exceptions;

import com.memory.colaborador.enums.MessageEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Getter
@NoArgsConstructor
public class NotFoundException extends ApiErrorException {
    public NotFoundException(MessageEnum messageEnum) {
        super(HttpStatus.NOT_FOUND, messageEnum);
    }
}
