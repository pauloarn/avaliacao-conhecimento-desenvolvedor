package com.memory.colaborador.service;

import com.memory.colaborador.utils.ModelMapperUtils;
import org.modelmapper.PropertyMap;
import org.springframework.data.domain.Page;

import java.util.List;

public abstract class AbstractConverter {

    private final ModelMapperUtils modelMapperUtils = new ModelMapperUtils();

    public <C, T> List<C> convertToListDTO(List<T> list, Class<C> clazz) {
        return modelMapperUtils.mapAll(list, clazz);
    }

    public <C, T> List<C> convertToListDTO(List<T> list, Class<C> clazz, PropertyMap<T, C> propertyMap) {
        return modelMapperUtils.mapAll(list, clazz, propertyMap);
    }

    public <C> C convertToSingleDTO(Object entity, Class<C> clazz) {
        return modelMapperUtils.map(entity, clazz);
    }

    public <P, C> C convertToSingleDTO(P entity, Class<C> clazz, PropertyMap<P, C> propertyMap) {
        return modelMapperUtils.map(entity, clazz, propertyMap);
    }

    public <P, C> Page<C> convertToPageDTO(Page<P> pageEntity, Class<C> clazz, PropertyMap<P, C> propertyMaps) {
        return pageEntity.map(en -> convertToSingleDTO(en, clazz, propertyMaps));
    }

    public <P, C> void map(P origem, C destino) {
        modelMapperUtils.map(origem, destino);
    }
}